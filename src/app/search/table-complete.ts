// import {DecimalPipe} from '@angular/common';
// import {Component, QueryList, ViewChildren} from '@angular/core';
// import {Observable} from 'rxjs';
// import { FormsModule } from '@angular/forms';

// import {search} from './search';
// import {CountryService} from './search.service';
// import {NgbdSortableHeader, SortEvent} from './sorttable.directive';


// @Component(
//     {selector: 'ngbd-table-complete', templateUrl: './search.component.html', providers: [CountryService, DecimalPipe]})
// export class NgbdTableComplete {
//   countries$: Observable<search[]>;
//   total$: Observable<number>;


//   @ViewChildren(NgbdSortableHeader)
//   headers!: QueryList<NgbdSortableHeader>;

//   constructor(public service: CountryService) {
//     this.countries$ = service.countries$;
//     this.total$ = service.total$;
//   }

//   onSort({column, direction}: SortEvent) {
//     // resetting other headers
//     this.headers.forEach(header => {
//       if (header.sortable !== column) {
//         header.direction = '';
//       }
//     });

//     this.service.sortColumn = column;
//     this.service.sortDirection = direction;
//   }
// }
