export interface search {
  id: number;
  name: string;
  flag: string;
  area: number;
  population: number;
}
